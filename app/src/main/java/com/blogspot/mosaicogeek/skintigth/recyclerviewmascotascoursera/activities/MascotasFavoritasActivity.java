package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo.ObjMascota;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters.AdapterMascota;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.fragments.RecyclerViewFragment;

import java.util.ArrayList;

public class MascotasFavoritasActivity extends AppCompatActivity {

    ArrayList<ObjMascota> listaFavoritas;
    private RecyclerView mascotasFavoritasRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_mascotas_favoritas_activity);

        //Agregar ActionBarPersonalizada
        Toolbar actionBar = (Toolbar) findViewById(R.id.MascotasFavoritasActivity_ActionBar);
        //Cambia el texto del ActionBar
        actionBar.setTitle(R.string.texto_boton_estrella);
        setSupportActionBar(actionBar);
        //Mostrar boton Up en el activity
        try{
            //noinspection ConstantConditions
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception ignored){
        }

        //llena la lista de favoritas
        listaFavoritas = new ArrayList<>();
        llenarListaFavoritas();

        //Asociar RecyclerView
        mascotasFavoritasRecyclerView = (RecyclerView) findViewById(R.id.MascotasFavoritas_RecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mascotasFavoritasRecyclerView.setLayoutManager(linearLayoutManager);
        iniciarAdapterFav();
    }

    //iniciar el adapter para las favoritas
    public void iniciarAdapterFav(){
        AdapterMascota adaptadorFavoritas = new AdapterMascota(listaFavoritas);
        mascotasFavoritasRecyclerView.setAdapter(adaptadorFavoritas);
    }

    //Lista de mascotas favoritas
    public void llenarListaFavoritas(){
        listaFavoritas = new ArrayList<>();

        listaFavoritas.add(RecyclerViewFragment.moonMoon);
        listaFavoritas.add(RecyclerViewFragment.doggyBravo);
        listaFavoritas.add(RecyclerViewFragment.garfield);
        listaFavoritas.add(RecyclerViewFragment.magestic);
        listaFavoritas.add(RecyclerViewFragment.fido);
    }

    //Se sobre escribe este metodo para que al presionar el boton UP del ActionBar
    //la activity padre que se finalizo pueda volver a iniciarse
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Metodo que inicia el MainActivity cuando se presiona el boton Back
    public void onBackPressed(){
        this.startActivity(new Intent(MascotasFavoritasActivity.this, MainActivity.class));
    }
}
