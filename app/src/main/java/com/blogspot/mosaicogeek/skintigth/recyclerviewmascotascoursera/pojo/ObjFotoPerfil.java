package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo;


//Clase para configurar los objetos de tipo ObjFotoPerfil
public class ObjFotoPerfil {

    //Caracteristicas del objeto
    int rating;
    int foto;

    //Constructor del objeto
    public ObjFotoPerfil(int rating, int foto) {
        this.rating = rating;
        this.foto = foto;
    }

    public int getRating() {
        return rating;
    }

    public int getFoto() {
        return foto;
    }
}
