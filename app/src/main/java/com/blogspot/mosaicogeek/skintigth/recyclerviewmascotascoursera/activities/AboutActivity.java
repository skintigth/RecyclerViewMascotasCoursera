package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_about_activity);

        //Agregar ActionBarPersonalizada
        Toolbar actionBar = (Toolbar) findViewById(R.id.AboutActivity_ActionBar);
        //Cambia el texto del ActionBar
        actionBar.setTitle(R.string.developer);
        setSupportActionBar(actionBar);
        //Mostrar boton Up en el activity
        try{
            //noinspection ConstantConditions
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception ignored){
        }

    }

    //Metodo que crea el menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_action_bar_about_activity, menu);
        return true;
    }

    //Metodo que se encarga de las funciones de la ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch(item.getItemId()){
            case R.id.menu_about_github_icon:
                //este intent lanza la pagina web de github de skintigth
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://github.com/skintigth"));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.menu_about_gplus_icon:
                //este instent lanza el perfil de google plus de skintigth
                Intent intent2 = new Intent(Intent.ACTION_VIEW);
                intent2.setData(Uri.parse("https://plus.google.com/+rurickmaqueo"));
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);
                break;
            case android.R.id.home:
                Intent intent3 = new Intent(this, MainActivity.class);
                startActivity(intent3);
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //Metodo que recupero la Main activity al presionar el boton Back
    public void onBackPressed(){
        this.startActivity(new Intent(AboutActivity.this, MainActivity.class));
    }
}
