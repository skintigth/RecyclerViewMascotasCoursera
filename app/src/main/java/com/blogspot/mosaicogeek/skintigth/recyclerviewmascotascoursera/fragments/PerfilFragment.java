package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters.AdapterFoto;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo.ObjFotoPerfil;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

public class PerfilFragment extends Fragment {

    private CircularImageView fotoPerfil;
    private TextView nombrePerfil;
    private RecyclerView reciclerViewPerfil;

    ArrayList<ObjFotoPerfil> fotos;

    //Objetos hardcodeados con las fotos de Moon Moon
    ObjFotoPerfil moon01 = new ObjFotoPerfil(5, R.drawable.moon01);
    ObjFotoPerfil moon02 = new ObjFotoPerfil(2, R.drawable.moon02);
    ObjFotoPerfil moon03 = new ObjFotoPerfil(9, R.drawable.moon03);
    ObjFotoPerfil moon04 = new ObjFotoPerfil(8, R.drawable.moon04);
    ObjFotoPerfil moon05 = new ObjFotoPerfil(3, R.drawable.moon05);
    ObjFotoPerfil moon06 = new ObjFotoPerfil(5, R.drawable.moon06);
    ObjFotoPerfil moon07 = new ObjFotoPerfil(4, R.drawable.moon07);
    ObjFotoPerfil moon08 = new ObjFotoPerfil(8, R.drawable.moon08);
    ObjFotoPerfil moon09 = new ObjFotoPerfil(2, R.drawable.moon09);
    ObjFotoPerfil moon10 = new ObjFotoPerfil(1, R.drawable.moon10);
    ObjFotoPerfil moon11 = new ObjFotoPerfil(6, R.drawable.moon11);
    ObjFotoPerfil moon12 = new ObjFotoPerfil(3, R.drawable.moon12);
    ObjFotoPerfil moon13 = new ObjFotoPerfil(2, R.drawable.moon13);
    ObjFotoPerfil moon14 = new ObjFotoPerfil(2, R.drawable.moon14);
    ObjFotoPerfil moon15 = new ObjFotoPerfil(5, R.drawable.moon15);
    ObjFotoPerfil moon16 = new ObjFotoPerfil(9, R.drawable.moon16);
    ObjFotoPerfil moon17 = new ObjFotoPerfil(1, R.drawable.moon17);


    public PerfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_perfil, container, false);

        //Llenar la lista de fotos
        fotos = new ArrayList<>();
        llenarListaFotos();

        //Lineas para hacer funcionar el RecyclerView
        reciclerViewPerfil = (RecyclerView) v.findViewById(R.id.PerfilFragment_RecyclerView);
        reciclerViewPerfil.setNestedScrollingEnabled(true);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),3);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        reciclerViewPerfil.setLayoutManager(layoutManager);
        iniciarAdapter();

        return v;
    }

    //Metodo que inicia el adapter
    public void iniciarAdapter(){
        AdapterFoto adapterFoto = new AdapterFoto(fotos);
        reciclerViewPerfil.setAdapter(adapterFoto);
    }

    //Metodo que llena la lista con las fotos
    public void llenarListaFotos(){
        fotos = new ArrayList<>();

        fotos.add(moon01);
        fotos.add(moon02);
        fotos.add(moon03);
        fotos.add(moon04);
        fotos.add(moon05);
        fotos.add(moon06);
        fotos.add(moon07);
        fotos.add(moon08);
        fotos.add(moon09);
        fotos.add(moon10);
        fotos.add(moon11);
        fotos.add(moon12);
        fotos.add(moon13);
        fotos.add(moon14);
        fotos.add(moon15);
        fotos.add(moon16);
        fotos.add(moon17);
    }

}
