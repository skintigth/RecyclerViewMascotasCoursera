package com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.R;
import com.blogspot.mosaicogeek.skintigth.recyclerviewmascotascoursera.pojo.ObjFotoPerfil;

import java.util.ArrayList;
import java.util.Locale;

public class AdapterFoto extends RecyclerView.Adapter<AdapterFoto.ViewHolderFoto>{

    //Lista de objetos foto
    private ArrayList<ObjFotoPerfil> fotos;

    //constructor
    public AdapterFoto(ArrayList<ObjFotoPerfil> fotos){
        this.fotos = fotos;
    }

    @Override
    public ViewHolderFoto onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_foto_card, parent, false);
        return new ViewHolderFoto(v);
    }


    //Le asigna los valores de la lista a los view que se reciclan
    @Override
    public void onBindViewHolder(AdapterFoto.ViewHolderFoto holder, int position) {
        //Objeto que clona al actual de la lista
        ObjFotoPerfil foto2 = fotos.get(position);
        //Se le indica al holder que use el recurso de la lista para llenar el view
        holder.foto.setImageResource(foto2.getFoto());
        //Se debe pasar el int a String para poder settear el texto
        holder.rating.setText(String.format(Locale.getDefault(), "%1$d", foto2.getRating()));
    }

    //Regresa la cantidad de elementos que tiene la lista
    @Override
    public int getItemCount() {
        return fotos.size();
    }

    //Una clase secundaria necesaria para el adaptador
    static class ViewHolderFoto extends RecyclerView.ViewHolder{

        //Views que se van a reciclar
        private ImageView foto;
        private TextView rating;

        //Este constructor es el que relaciona los componentes
        //de los objetos con los elementos del layout
        ViewHolderFoto(final View itemView) {
            super(itemView);
            //Relacionamos los Views a reciclar con los respectivos ID de la tarjeta
            foto = (ImageView) itemView.findViewById(R.id.PerfilMascota_Imagen);
            rating = (TextView) itemView.findViewById(R.id.PerfilMascota_Rating);
        }
    }
}
